package util;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadEcel {
	static Object[][] data;

	public static Object[][] readData(String dataSheetname) {
		XSSFWorkbook wbook;
		try {
			 wbook=new XSSFWorkbook("./Data/"+dataSheetname+".xlsx");
			XSSFSheet sheet = wbook.getSheet("Sheet1");
			int lastRowNum = sheet.getLastRowNum();
			System.out.println("Count of Row"+lastRowNum);
			short colcount = sheet.getRow(0).getLastCellNum();
			System.out.println("Count of Coloumn"+colcount);
			data = new Object[lastRowNum][colcount];

			for(int i=1;i<=lastRowNum;i++) {
				XSSFRow row = sheet.getRow(i);
				for(int j=0;j<colcount;j++)
				{
				XSSFCell cell = row.getCell(j);
				data[i-j][j]=cell.getStringCellValue();
				String text = cell.getStringCellValue();
				System.out.println(text);
				
				}
				wbook.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	

}

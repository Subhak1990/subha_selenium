package util;

import org.junit.BeforeClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class AdvanceReports  {
	
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	public String testcaseName;
	public String testDec;
	public String author;
	public String category;

	//public void reunReport() {
	@BeforeSuite
	public void startReport()
	{
		html = new ExtentHtmlReporter("./reports/extentReport.html");
		extent=new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
	}
	
	@BeforeClass
	public void initializeTest(String testcaseName,String testDec, String author, String category) 
	
	{
		test=extent.createTest(testcaseName,testDec);
		test.assignAuthor(author);
		test.assignCategory(category);
    }
	
	
	public void logStep(String des, String status) {
		if (status.equalsIgnoreCase("pass")) {
			test.pass(des);
		} else if (status.equalsIgnoreCase("fail")) {
			test.fail(des);
			throw new RuntimeException();
		} else if (status.equalsIgnoreCase("warning")) {
			test.warning(des);
		}
		}
	@AfterSuite
	public void endReport() {
	extent.flush();
	
	}


}

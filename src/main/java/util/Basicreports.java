package util;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Basicreports {
 
	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void reunReport() {
		
		html=new ExtentHtmlReporter("./reports/extentReport.html");
		extent=new ExtentReports();
		html.setAppendExisting(true);
		extent.attachReporter(html);
		test=extent.createTest("TC001_Login","Login case");
		test.assignAuthor("Subha");
		test.assignCategory("Smoke");
		try {
			test.pass("passed successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			e.printStackTrace();
		}
		extent.flush();
	}
	
}

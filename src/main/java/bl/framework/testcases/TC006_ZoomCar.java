package bl.framework.testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TC006_ZoomCar {
	@Test
	void bookJournry() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver= new ChromeDriver(); 	
	    driver.get("https://www.zoomcar.com/chennai");
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    driver.findElementByClassName("search").click();
	    driver.findElementByXPath("//div[text()='Popular Pick-up points']/following-sibling::div").click();
	    driver.findElementByClassName("proceed").click();
	 // Get the current date
		
	    Date date = new Date();	    		
	    DateFormat sdf = new SimpleDateFormat("dd");    
	    		
	    String today = sdf.format(date);
     
	    int tomorrow = Integer.parseInt(today)+1;
	    System.out.println(tomorrow);    
	    	
	    driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
	    
	    driver.findElementByClassName("proceed").click();
	    driver.findElementByClassName("proceed").click();
        //List<WebElement> carList = driver.findElementsByXPath("//div[@class='car-list-layout']");
        List<WebElement> priceList = driver.findElementsByXPath("//div[@class='price']");
        List<String> priceList1=new ArrayList<>();
        for(int i=0;i<=priceList.size()-1;i++) {
        	 priceList1.add(priceList.get(i).getText().replaceAll("\\D", ""));
         
        }
        System.out.println(priceList1);
        String max = Collections.min(priceList1);
        System.out.println(max);
        String textBN = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/parent::div/parent::div/preceding-sibling::div[1]/h3").getText();
        System.out.println(textBN);
        driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following-sibling::div//following-sibling::button").click();
        driver.close();

	    
	    
	}

}

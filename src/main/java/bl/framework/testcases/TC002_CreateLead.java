package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import projectMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods
{
	//CreateLead is an in
    @Test
	void createLead()
	{
    	//login();
		WebElement eleCreateLead = locateElement("visibleText", "Create Lead");
		click(eleCreateLead);
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompName, "Infosys");
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, "Subha");
		WebElement eleLasrName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLasrName, "Vijay");
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		//selectDropDownUsingText(eleSource, "Cold Call");
		int optionSize = getOptionSize(eleSource);
		//To find the size
		selectDropDownUsingIndex(eleSource, optionSize-2);
		
		WebElement eleSubmit = locateElement("name", "submitButton");
		click(eleSubmit);
		
		
	}
}

package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import projectMethods.ProjectMethods;
import util.ReadEcel;

public class TC008_CreateLead extends ProjectMethods
{
	
	@BeforeTest
	public void setData()
	{
		testcaseName="Create Lead";
		testDec="Log";
		author="Subha";
	}
	//CreateLead is an in
    @Test(dataProvider="Fetch1")
	 public void createLead(String compN,String Fname, String Lname)
	{
    	
    	
		WebElement eleCreateLead = locateElement("visibleText", "Create Lead");
		click(eleCreateLead);
		WebElement eleCompName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompName, compN);
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, Fname);
		WebElement eleLasrName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLasrName, Lname);
		//WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		//selectDropDownUsingText(eleSource, "Cold Call");
		//int optionSize = getOptionSize(eleSource);
		//To find the size
		//selectDropDownUsingIndex(eleSource, optionSize-2);
		
		WebElement eleSubmit = locateElement("name", "submitButton");
		click(eleSubmit);
		
		
	}
    @DataProvider(name="FirstSet")
    public Object[][] fetchdata()
    {
    	
    	return ReadEcel.readData(dataSheetname);
    	
    			
    }
    
//    public String[][] fetchdata1()
//    {
//    	String[][] data=new String[2][3];
//    	data[0][0]="Test Leaf";
//    	data[0][1]="Sarath";
//    	data[0][2]="2";
//    	
//    	data[1][0]="Test Leaf";
//    	data[1][1]="Gayathri";
//    	data[1][2]="2";
//    	return data;
//    	
//    			
//    }
//   
}

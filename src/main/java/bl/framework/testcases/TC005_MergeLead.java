package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import projectMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods
{
	@Test
void mergeLead() throws InterruptedException {
	//login();
    WebElement element5 = locateElement("visibleText",  "Leads");
    click(element5);
    
    WebElement element6 = locateElement("visibleText", "Merge Leads");
    click(element6);
     WebElement element7 = locateElement("xpath", "//input[@id='partyIdFrom']/following-sibling::a");
     click(element7);
     Thread.sleep(3000);

	 switchToWindow(1);
     WebElement element8= locateElement("xpath", "//label[text()='Lead ID:']//following::div/input");
     clearAndType(element8, "10086");
     WebElement element9 = locateElement("xpath", "//button[@class='x-btn-text']");
     click(element9);
     
     WebElement element10 = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
     click(element10);
     switchToWindow(0);
     WebElement element12 = locateElement("id", "ComboBox_partyIdTo");
     clearAndType(element12, "10084");
     WebElement element11= locateElement("xpath", "//a[@class='buttonDangerous']");
     click(element11);
     switchToWindow(1);
     click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
 	switchToWindow(1);
 	clearAndType(locateElement("xpath", "//label[text()='Lead ID:']//following::div/input"),"10090"); 
 	click(locateElement("visibleText", "Find Leads"));
 	click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']"));
     
}
}

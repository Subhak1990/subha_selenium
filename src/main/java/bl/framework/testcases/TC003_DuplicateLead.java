package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import projectMethods.ProjectMethods;

public class TC003_DuplicateLead extends ProjectMethods {
	 @Test
	void duplicateLead() throws InterruptedException{
    //login();
    
    WebElement element5 = locateElement("visibleText", "Create Lead");
    click(element5);
    WebElement element6 = locateElement("visibleText", "Find Leads");
    click(element6);
    WebElement element7 = locateElement("visibleText", "Email");
    click(element7);
    WebElement element8 = locateElement("name", "emailAddress");
    clearAndType(element8, "sss@sss.com");
    WebElement element9 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
    click(element9);
    Thread.sleep(3000);
    WebElement element10 = locateElement("xpath", "(//td[@class= 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
    click(element10);
    WebElement element11 = locateElement("id", "viewLead_firstName_sp");
    String expText = getElementText(element11);
    WebElement element14 = locateElement("class", "subMenuButton");
    click(element14);

    //WebElement element12 = locateElement("id", "(//div[@class='x-panel-tc']/div)[22]");
    //WebElement element12 = locateElement("class", "smallSubmit");
   // click(element12);
    verifyTitle("Duplicate Lead | opentaps CRM");
    click(locateElement("name", "submitButton"));
    WebElement element13 = locateElement("id", "viewLead_firstName_sp");
    boolean value = verifyExactText(element13, expText);
    System.out.println(value);
      
    
    
    
    
    
    
    
    }

}

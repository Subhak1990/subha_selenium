package bl.framework.testcases;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import projectMethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods {
     @Test
	void editLead() {
		//3login();
	    WebElement element5 = locateElement("visibleText", "Create Lead");
	    click(element5);
	    WebElement element6 = locateElement("visibleText", "Find Leads");
	    click(element6);
	    WebElement element7 = locateElement("visibleText", "Email");
	    click(element7);
	    WebElement element8 = locateElement("name", "emailAddress");
	    clearAndType(element8, "sss@sss.com");
	    WebElement element9 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
	    click(element9);
	    WebElement element10 = locateElement("xpath", "(//td[@class= 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]");
	    click(element10);
	    WebElement element11 = locateElement("id", "viewLead_firstName_sp");
	    getElementText(element11);
	    WebElement element12 = locateElement("id", "viewLead_companyName_sp");
	    getElementText(element12);
	    WebElement element13 = locateElement("visibleText", "Edit");
	    click(element13);
	    WebElement element14 = locateElement("id", "sectionHeaderTitle_leads");
	    getElementText(element14);
	    WebElement element15 = locateElement("id", "updateLeadForm_companyName");
	    clearAndType(element15, "KEk");
	    WebElement element16 = locateElement("xpath", "(//input[@class='smallSubmit'])[1]");
	    click(element16);
	    WebElement element17 = locateElement("id", "viewLead_companyName_sp");
	    String elementText = getElementText(element17);
verifyPartialText(element16, elementText);

	    
		
	}
}

package bl.framework.api;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.management.RuntimeErrorException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import util.AdvanceReports;

public class SeleniumBase extends AdvanceReports implements Browser, Element{

	public RemoteWebDriver driver;
	public int i =1;
	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
			System.err.println("Driver Exception");
		}
		finally
		{
			takeSnap();
		
		}
	}

	

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "xpath": return driver.findElementByXPath(value);
			case "link":return driver.findElementByLinkText(value);
			case "visibleText":return driver.findElementByPartialLinkText(value);
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			System.err.println("The value"+value+"is not present");
		}
		catch(WebDriverException e) {
			System.err.println("Error unknown");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "xpath": return driver.findElementsByXPath(value);
			case "link":return driver.findElementsByLinkText(value);
			case "visibleText":return driver.findElementsByPartialLinkText(value);
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			System.err.println("The value"+value+"is not present");
		}
		catch(WebDriverException e) {
			System.err.println("Error unknown");
		}
		

		return null;
	}

	@Override
	public void switchToAlert() {
try {
	driver.switchTo().alert();
} catch (NoAlertPresentException e) {
	System.err.println("No such alert is present");
	}
catch(UnhandledAlertException e1)
{
	System.err.println("No Alert to be handled");
}
finally
{
	takeSnap();
}
	}

	@Override
	public void acceptAlert() {
     try {
		driver.switchTo().alert().accept();
	} catch (NoAlertPresentException e) {
     System.err.println("No Alert is present");
	}catch(UnhandledAlertException e1) {
		System.err.println("No Alert is pesent to be handled");
	}
     finally {
    	 takeSnap();
     }
	}

	@Override
	public void dismissAlert() {
try {
	driver.switchTo().alert().dismiss();
} 
catch(NoAlertPresentException e)
{
	System.err.println("No alert like that is present");
}
catch (UnhandledAlertException e) {
System.err.println("Alert is not handled");}
finally {
	takeSnap();
}
	}

	@Override
	public String getAlertText() {
		String text = "";
      try {
		Alert alert = driver.switchTo().alert();
		   text = alert.getText();
		   System.out.println(text);
	} catch (NoAlertPresentException e) {
           System.err.println("The alert is not present");		
	}
      finally {
		takeSnap();
	}

	return text;
	}

	@Override
	public void typeAlert(String data) {
		try
		{
    driver.switchTo().alert().sendKeys(data);
		}
		catch(WebDriverException e)
		{
			System.err.println("Driver Exception");
		}
		finally
		{
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> ls=new ArrayList<String>();
			ls.addAll(allWindows);
			driver.switchTo().window(ls.get(index));
		} catch (NoSuchWindowException e) {
			System.err.println("Winow is not present");
		}finally
		{
		takeSnap();
		}
	}

	@Override
	public void switchToWindow(String title) {
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> ls=new ArrayList<String>();
			ls.addAll(allWindows);
			driver.switchTo().window(title);
		} catch (NoSuchWindowException e) {
			System.err.println("Winow is not present");
		}finally
		{
		takeSnap();
		}
	
	}

	@Override
	public void switchToFrame(int index) {
		try {
       driver.switchTo().frame(index);
		}
		catch(NoSuchFrameException e)
		{
			System.err.println("Frame is not present");
		}
		finally
		{
			takeSnap();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
		       driver.switchTo().frame(ele);
				}
				catch(NoSuchFrameException e)
				{
					System.err.println("Frame is not present");
				}
				finally
				{
					takeSnap();
				}

	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
		       driver.switchTo().frame(idOrName);
				}
				catch(NoSuchFrameException e)
				{
					System.err.println("Frame is not present");
				}
				finally
				{
					takeSnap();
				}

	}

	@Override
	public void defaultContent() {
driver.switchTo().defaultContent();
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}
    
	

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
     try {
		driver.close();
	} catch (WebDriverException e) {
		System.err.println("Exception");
	}
	}

	@Override
	public void quit() {
		try {
driver.quit();
System.out.println("All the browsers will be closed");
		}
		catch(WebDriverException e) {
			System.err.println("Driver exception");
		}
		finally
		{
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The element "+ele+" clicked successfully");
		takeSnap();
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
    ele.clear();		
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data); 
			System.out.println("The data "+data+" entered successfully");
		} catch (NoSuchElementException e) {
			System.err.println("No such element is present");
		}
		finally
		{
		takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		String text="";
		try {
	 text = ele.getText();
	System.out.println("The text is "+i+ text);
	i++;
		}
		catch(NoSuchElementException e)
		{
			System.out.println("No such element is present");
		}
		catch(WebDriverException e) {
			System.err.println("Driver Exception");
		}
		finally
		{
			takeSnap();
		}
	return text;
	
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		String text="";
		try {
		    text = ele.getText();
			System.out.println(text);
		} catch (NoSuchElementException e) {
              System.err.println("The element is not present");
		}
		  catch(WebDriverException e) {
			  System.err.println("Driver is not present");
		  }
		finally
		{
			takeSnap();
		}
		return text;

	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
     Select sel=new Select(ele);
     sel.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select sel=new Select(ele);
	    sel.selectByIndex(index);
	     
		
	}
	 public int getOptionSize(WebElement sizeOp) {
		 Select sel=new Select(sizeOp);		 
		 
		return sel.getOptions().size();
	 }

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select sel=new Select(ele);
	    sel.selectByValue(value);
	     
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
  try {
	String text = ele.getText();
	  if(text.equalsIgnoreCase(expectedText))
	  {
			System.out.println("Yes:  "+text+" is same as expected : "+expectedText);
			return true;
		}
		else
		{
			System.out.println("No: "+text+" is not same as expected : "+expectedText);
			return false;
		}
} catch (NullPointerException  e) {
	System.err.println("web element is null");
	}
    catch(NoSuchElementException e) {
	System.err.println("No element is present like that");
}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if(text.contains(expectedText))	{
				System.out.println("Actual Text: "+ text);
				return true;
			}
			else {
				System.out.println("Expected Text is: "+expectedText+" Actual Text is: "+text);
				return false;
			}
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }	
    	return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {

		String curTitle1 = driver.getTitle();
		if (title.equals(curTitle1)) {
			System.out.println(curTitle1+" equal to"+title);
		}else {
        System.out.println(curTitle1+" is not equal to"+title);
			}
        takeSnap();		return false;
	}

}

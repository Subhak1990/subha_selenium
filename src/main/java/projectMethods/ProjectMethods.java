package projectMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.model.Author;

import bl.framework.api.SeleniumBase;
import util.ReadEcel;

public class ProjectMethods extends SeleniumBase 
{
	public String dataSheetname;

	
	
	@BeforeMethod
	public void login() {

		startApp("chrome", "http://leaftaps.com/opentaps/control/login");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		WebElement eleCrmsfa = locateElement("visibleText", "CRM/SFA");
		click(eleCrmsfa);
		
	}
	
	@AfterMethod
	public void close() {
		quit();
	}
	@DataProvider(name="Fetch1")
	public Object[][] getData() {
		 return ReadEcel.readData("UseCase1");
		 
		
	}

}

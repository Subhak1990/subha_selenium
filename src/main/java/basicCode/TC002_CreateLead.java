package basicCode;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TC002_CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver= new ChromeDriver(); 	
	    driver.get("http://leaftaps.com/opentaps/control/main");
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    driver.findElementById("username").sendKeys("DemoSalesManager");
	    driver.findElementById("password").sendKeys("crmsfa");
	    driver.findElementByClassName("decorativeSubmit").click();
	    driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Create Lead").click();
	    driver.findElementById("createLeadForm_companyName").sendKeys("Infosys");
	    driver.findElementById("createLeadForm_firstName").sendKeys("Gautham");
	    driver.findElementById("createLeadForm_lastName").sendKeys("k");
	    WebElement ele1 = driver.findElementById("createLeadForm_dataSourceId");
	    Select sel1=new Select(ele1);
	    sel1.selectByIndex(1);
	    driver.findElementById("createLeadForm_primaryEmail").sendKeys("sss@sss.com");
	    driver.findElementByXPath("//input[@class='smallSubmit']").click();
			}

}

package basicCode;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class TC003_DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver= new ChromeDriver(); 	
	    driver.get("http://leaftaps.com/opentaps/control/main");
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    driver.findElementById("username").sendKeys("DemoSalesManager");
	    driver.findElementById("password").sendKeys("crmsfa");
	    driver.findElementByClassName("decorativeSubmit").click();
	    driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Create Lead").click();
	    driver.findElementByLinkText("Find Leads").click();
	    driver.findElementByLinkText("Email").click();
	    driver.findElementByName("emailAddress").sendKeys("sss@sss.com");
	    driver.findElementById("ext-gen334").click();
	    Thread.sleep(3000);
	    driver.findElementByXPath("(//td[@class= 'x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]").click();
	    String text2 = driver.findElementById("viewLead_firstName_sp").getText();
	    System.out.println("text1 is "+text2);
	    //String text = driver.findElementById("(//div[@class='x-panel-tc']/div)[22]").getText();
	   // System.out.println("header "+text);
	    driver.findElementByPartialLinkText("Duplicate Lead").click();
	    
	    String title = driver.getTitle();
	    System.out.println(title);
	    driver.findElementByClassName("smallSubmit").click();
	    
	    
	    String text = driver.findElementById("viewLead_firstName_sp").getText();
	    System.out.println("Text2 :"+text);
	    if(text2.equals(text))
	    {
	    	System.out.println("Testcase is pass");
	    }
	    else
	    {
	    	System.out.println("Testcase is fail");
	    }


	    
	}

}


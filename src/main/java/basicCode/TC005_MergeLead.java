package basicCode;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class TC005_MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    ChromeDriver driver= new ChromeDriver(); 	
	    driver.get("http://leaftaps.com/opentaps/control/main");
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.manage().window().maximize();
	    driver.findElementById("username").sendKeys("DemoSalesManager");
	    driver.findElementById("password").sendKeys("crmsfa");
	    driver.findElementByClassName("decorativeSubmit").click();
	    driver.findElementByLinkText("CRM/SFA").click();
	    driver.findElementByLinkText("Leads").click();
	    driver.findElementByLinkText("Merge Leads").click();
	    Thread.sleep(3000);
	    driver.findElementByXPath("//input[@id='partyIdFrom']/following-sibling::a").click();
	    Set<String> Allwindows = driver.getWindowHandles();
	    List<String> ls=new ArrayList<String>();
	    ls.addAll(Allwindows);
	   // driver.switchTo().window(ls.get(0));
	   // driver.findElementByXPath("//input[@id='partyIdFrom']/following-sibling::a").click();
	   
	    driver.switchTo().window(ls.get(1));
	    
	    driver.findElementByXPath("(//input[@class=' x-form-text x-form-field '])[1]").sendKeys("10022");
	}

}
